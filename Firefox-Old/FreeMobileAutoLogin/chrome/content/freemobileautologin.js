if (! com) var com = {};
if (! com.abdesmok) com.abdesmok = {};

com.abdesmok.freemobileautologin = {
    ocr: function (doc) {
        if (this['ocrDoc'] != doc) {
            this.ocrDoc = doc;
            
            var predefinedDigits = [5, 8, 2, 6, 7, 4, 3, 9, 1, 0];
            
            var predefinedBits = [
                [ 495, -8, 32, -8, 32, -7, 33, -2, 38, -2, 38, -7, 33, -8, 32, -3, 2, -3, 37, -3, 37, -3, 32, -2, 3, -3, 32, -2, 3, -3, 32, -3, 2, -3, 32, -8, 33, -6, 538, ],
                [ 497, -4, 35, -6, 33, -3, 2, -3, 32, -3, 2, -3, 32, -3, 2, -3, 32, -3, 2, -3, 33, -6, 34, -6, 33, -3, 2, -3, 32, -3, 2, -3, 32, -3, 2, -3, 32, -3, 2, -3, 32, -3, 2, -3, 32, -8, 33, -6, 538, ],
                [ 496, -5, 34, -7, 33, -3, 1, -4, 32, -3, 2, -3, 32, -2, 3, -3, 37, -3, 36, -4, 35, -4, 35, -4, 35, -4, 35, -4, 36, -3, 37, -3, 37, -8, 32, -8, 537, ],
                [ 497, -5, 34, -7, 32, -4, 1, -3, 32, -3, 3, -2, 32, -3, 37, -7, 33, -8, 32, -3, 2, -3, 32, -3, 2, -3, 32, -3, 2, -3, 32, -3, 2, -3, 32, -3, 2, -3, 32, -3, 2, -3, 32, -8, 33, -6, 538, ],
                [ 495, -8, 31, -9, 32, -8, 37, -3, 36, -3, 37, -3, 37, -2, 37, -3, 37, -3, 37, -2, 37, -3, 37, -3, 37, -3, 37, -3, 37, -3, 540, ],
                [ 499, -3, 36, -4, 36, -4, 35, -5, 35, -5, 34, -2, 1, -3, 34, -2, 1, -3, 33, -3, 1, -3, 33, -2, 2, -3, 32, -3, 2, -3, 32, -9, 31, -9, 36, -3, 37, -3, 37, -3, 538, ],
                [ 496, -5, 34, -7, 33, -3, 2, -3, 32, -2, 3, -3, 37, -3, 36, -3, 35, -5, 35, -5, 38, -3, 37, -3, 31, -3, 3, -3, 31, -3, 3, -3, 32, -3, 2, -3, 32, -7, 34, -5, 539, ],
                [ 496, -6, 33, -8, 32, -8, 32, -3, 2, -3, 32, -3, 2, -3, 32, -3, 2, -3, 32, -3, 2, -3, 32, -3, 2, -3, 32, -8, 33, -7, 37, -3, 37, -3, 32, -3, 2, -3, 32, -8, 33, -6, 538, ],
                [ 499, -2, 37, -3, 34, -6, 34, -6, 34, -6, 37, -3, 37, -3, 37, -3, 37, -3, 37, -3, 37, -3, 37, -3, 37, -3, 37, -3, 37, -3, 539, ],
                [ 497, -4, 35, -6, 33, -3, 1, -4, 32, -3, 2, -3, 32, -3, 2, -3, 32, -3, 2, -3, 32, -3, 2, -3, 32, -3, 2, -3, 32, -3, 2, -3, 32, -3, 2, -3, 32, -3, 2, -3, 32, -3, 2, -3, 32, -3, 2, -3, 32, -8, 33, -6, 538, ],
            ];
            
            /*function bitsToStr(bits) {
                var str = "[ ";
                for (var i = 0; i < bits.length; ++i) {
                    str += bits[i] + ", ";
                }
                str += "],";
                return str;
            }*/
            
            function uncompressBits(bits) {
                var uncompressed = [];
                for (var i = 0; i < bits.length; ++i) {
                    var bit = bits[i] > 0 ? 1 : 0;
                    for (var j = 0; j < bits[i] * (2 * bit - 1); ++j) {
                        uncompressed.push(bit);
                    }
                }
                return uncompressed;
            }
            
            var canvas, context;
            
            /*canvas = doc.createElement('canvas');
            
            doc.body.insertBefore(canvas, doc.body.firstChild);
            
            canvas.width = 400;
            canvas.height = 40;
            context = canvas.getContext('2d');
            
            for (var i = 0; i < predefinedBits.length; ++i) {
                var bits = uncompressBits(predefinedBits[i]);
                dst = context.createImageData(40, 40);
                for (var j = 0; j < bits.length; ++j) {
                    var gray = bits[j] * 255;
                    dst.data[j * 4 + 0] = gray;
                    dst.data[j * 4 + 1] = gray;
                    dst.data[j * 4 + 2] = gray;
                    dst.data[j * 4 + 3] = 255;
                }
                context.putImageData(dst, i * 40, 0);
            }*/
            
            canvas = doc.createElement('canvas');
            
            //doc.body.insertBefore(canvas, doc.body.firstChild);
            
            canvas.width = 400;
            canvas.height = 40;
            context = canvas.getContext('2d');
            
            var digits = [];
            
            this.imgs = doc.getElementsByClassName("ident_chiffre_img");
            
            for (var index = 0; index < this.imgs.length; ++index) {
                var src = this.imgs[index];
                
                context.drawImage(src, index * 40, 0);
                
                src = context.getImageData(index * 40, 0, 40, 40);
                
                //var dst = context.createImageData(40, 40);
                
                var bits = [];
                var previousBit = -1;
                var bitCount = 0;
                
                for (var y = 0; y < src.height; ++y) {
                    for (var x = 0; x < src.width; ++x) {
                        var i = x * 4 + y * 4 * src.width;
                        
                        var luma = Math.floor(
                            src.data[i] * 299 / 1000 +
                            src.data[i + 1] * 587 / 1000 +
                            src.data[i + 2] * 114 / 1000
                        );
                        
                        var bit = luma >= 230 ? 1 : 0;
                        
                        if (previousBit == bit) {
                            ++bitCount;
                        }
                        else {
                            if (previousBit != -1) {
                                bits.push((bitCount * (2 * previousBit - 1)));
                            }
                            
                            previousBit = bit;
                            bitCount = 1;
                        }
                        
                        /*dst.data[i] = 255 * bit;
                        dst.data[i + 1] = 255 * bit;
                        dst.data[i + 2] = 255 * bit;
                        dst.data[i + 3] = 255;*/
                    }
                }
                
                if (previousBit != -1) {
                    bits.push((bitCount * (2 * previousBit - 1)));
                }
                
                //context.putImageData(dst, index * 40, 0);
                
                //console.log(bitsToStr(bits));
                
                bits = uncompressBits(bits);
                
                var bestPerc = 0;
                var best = 0;
                
                for (var i = 0; i < predefinedBits.length; ++i) {
                    var count = 0;
                    var countOk = 0;
                    var bits2 = uncompressBits(predefinedBits[i]);
                    for (var j = 0; j < bits.length && j < bits2.length; ++j) {
                        if (bits[j] == 0 || bits2[j] == 0) {
                            ++count;
                            if (bits[j] == bits2[j]) {
                                ++countOk;
                            }
                        }
                    }
                    var perc = countOk * 100 / count;
                    
                    if (perc > bestPerc) {
                        bestPerc = perc;
                        best = predefinedDigits[i];
                    }
                    
                    //console.log(index + ": compare with " + predefinedDigits[i] + " " + perc + "%");
                }
                
                //console.log(best + " " + perc + "%");
                
                digits.push(best);
                
                //context.fillText("" + best, index * 40, 38);
            }
            
            //console.log(digits);
            
            this.digits = digits;
        }
        
        return this.digits;
    },
    
    decode: function (doc) {
        var autologin = this;
        
        this.imgs = doc.getElementsByClassName("ident_chiffre_img");
        
        for (var i = 0; i < this.imgs.length; ++i) {
            var src = this.imgs[i];
            
            if (src.naturalWidth === 0 || src.naturalHeight === 0 || src.complete === false) {
                //console.error("image " + i + " is broken")
                setTimeout(function() {
                    autologin.decode(doc);
                }, 500);
                return;
            }
        }
        
        var digits = this.ocr(doc);
        var codeText = this.codeText(doc);
        
        var code = [];
        for (var i = 0; i < codeText.length; ++i) {
            var digit = parseInt(codeText[i]);
            if (digit != NaN && digit >= 0 && digit <= 9) {
                code.push(digit);
            }
        }
        
        var button = doc.getElementById("ident_btn_cancel");
        if (button != null)
            button.click();
        
        for (var i = 0; i < this.imgs.length; ++i) {
            this.imgs[i].style.border = "";
        }
        
        var decode = function (index, delay) {
            setTimeout(
                function () {
                    //console.log(index);
                    autologin.imgs[index].style.border = "solid 1px black";
                    autologin.imgs[index].click();
                },
                delay);
        }
        
        for (var i = 0; i < code.length; ++i) {
            for (var j = 0; j < digits.length; ++j) {
                if (code[i] == digits[j]) {
                    decode(j, i * 50);
                    break;
                }
            }
        }
    },
    
    update: function (doc) {
        if (this["updatedDoc"] != doc) {
            this.updatedDoc = doc;
            
            var cancel = doc.getElementById("ident_btn_cancel");
            var parent = cancel ? cancel.parentNode : null;
            
            if (parent) {
                var change = doc.createElement("input");
                change.setAttribute("id", "ident_btn_change_code");
                change.setAttribute("type", "button");
                change.setAttribute("value", "Modifier l'identifiant");
                change.setAttribute("class", cancel.getAttribute("class"));
                change.setAttribute("style", cancel.getAttribute("style"));
                var autologin = this;
                cancel.addEventListener("click", function (event) {
                    if (autologin['imgs']) {
                        for (var i = 0; i < autologin.imgs.length; ++i) {
                            autologin.imgs[i].style.border = "";
                        }
                    }
                }, false);
                change.addEventListener("click", function (event) {
                    autologin.changeCode(doc);
                }, false);
                parent.insertBefore(change, cancel);
            }
            
            var submit = doc.getElementById("ident_btn_submit");
            var parent = submit ? submit.parentNode : null;
            
            if (parent) {
                var time = doc.createElement("input");
                time.setAttribute("type", "button");
                time.setAttribute("disabled", "");
                time.setAttribute("class", submit.getAttribute("class"));
                time.setAttribute("style", submit.getAttribute("style"));
                var computedStyle = window.getComputedStyle(submit);
                time.style.marginRight = computedStyle.marginRight;
                delete computedStyle;
                time.style.minWidth = submit.offsetWidth + "px";
                parent.insertBefore(time, submit);
                submit.style.display = "none";
                
                var ns = { time: time, submit: submit };
                
                var timer = function (i) {
                    setTimeout(function () {
                        ns.time.setAttribute("value", i + (i > 1 ? " secondes" : " seconde"));
                    }, (5 - i) * 1000);
                };
                
                for (var i = 5; i > 0; --i) {
                    timer(i);
                }
                
                setTimeout(function () {
                    ns.time.style.display = "none";
                    ns.submit.style.display = "";
                }
                , 5000);
            }
        }
        
        this.decode(doc);
    },
    
    codeText: function (doc) {
        var prefManager = Components.classes["@mozilla.org/preferences-service;1"].getService(Components.interfaces.nsIPrefService).getBranch("extensions.freemobileautologin.");
        var codeText = prefManager.getCharPref("identifiant");
        
        if (! codeText && ! this["prompted"]) {
            codeText = prompt("Entrez votre identifiant Free mobile :");
            this.prompted = true;
            
            if (codeText != null) {
                prefManager.setCharPref("identifiant", codeText);
            }
            else {
                codeText = "";
            }
        }
        
        return codeText;
    },
    
    changeCode: function (doc) {
        var prefManager = Components.classes["@mozilla.org/preferences-service;1"].getService(Components.interfaces.nsIPrefService).getBranch("extensions.freemobileautologin.");
        var codeText = prompt("Entrez votre identifiant Free mobile :", prefManager.getCharPref("identifiant"));
        this.prompted = true;
        
        if (codeText != null) {
            prefManager.setCharPref("identifiant", codeText);
            this.update(doc);
        }
    }
}

window.addEventListener("DOMContentLoaded", function (event) {
    if (event.originalTarget instanceof HTMLDocument && event.originalTarget.location.href.match("mobile.free.fr/moncompte")) {
        com.abdesmok.freemobileautologin.update(event.originalTarget);
    }
}, true);
