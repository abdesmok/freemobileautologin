function setupConnection() {
    opera.extension.onconnect = function(event) {
        event.source.postMessage({key : "identifiant", value: widget.preferences['identifiant']});
    }
    
    opera.extension.onmessage = function(event) {
        if (event.data.key === "identifiant") {
            widget.preferences['identifiant'] = event.data.value;
        }
    }
}

window.addEventListener("load", setupConnection, false);
