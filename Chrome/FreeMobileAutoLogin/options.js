// Saves options to localStorage.
function save_options() {
    var input = document.getElementById("identifiant");
    
    chrome.storage.sync.set({'identifiant': input.value}, function() {
        // Update status to let user know options were saved.
        var status = document.getElementById("status");
        
        status.innerHTML = "Sauvegardé";
        setTimeout(function() {
            status.innerHTML = "";
        }, 1000);
    });
}

// Restores select box state to saved value from localStorage.
function restore_options() {
    chrome.storage.sync.get("identifiant", function(val) {
        if (! val.identifiant) {
            return;
        }
        var input = document.getElementById("identifiant");
        input.value = val.identifiant;
    });
}

document.addEventListener("DOMContentLoaded", function() {
    restore_options();
    
    document.querySelector('#save').addEventListener("click", save_options);
});
